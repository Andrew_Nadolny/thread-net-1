﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.BLL.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<CommentHub> _commmentHub;

        public CommentService(ThreadContext context, IMapper mapper, IHubContext<CommentHub> commmentHub) : base(context, mapper) {
            _commmentHub = commmentHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);
            var createdCommentDTO = _mapper.Map<CommentDTO>(createdComment);

            await _commmentHub.Clients.All.SendAsync("NewComment", createdCommentDTO);

            return _mapper.Map<CommentDTO>(createdComment);
        }
        public async Task<CommentDTO> EditComment(EditedCommentDTO editedCommentDTO)
        {
            var commentEntity = _mapper.Map<Comment>(editedCommentDTO);
            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();

            var editedComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            var commentDTO = _mapper.Map<CommentDTO>(editedComment);

            await _commmentHub.Clients.All.SendAsync("EditedComment", commentDTO);

            return _mapper.Map<CommentDTO>(editedComment);
        }
        public async Task<bool> DeleteComment(int Id, int authorId)
        {
            var deletedComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == Id);
            if (authorId == deletedComment?.AuthorId)
            {
                var commentReactions = _context.CommentReactions.Where(x => x.CommentId == deletedComment.Id);
                _context.CommentReactions.RemoveRange(commentReactions);
                _context.Comments.Remove(deletedComment);
                await _context.SaveChangesAsync();

                var createdCommentDTO = _mapper.Map<CommentDTO>(deletedComment);

                await _commmentHub.Clients.All.SendAsync("DeletedComment", deletedComment);
                return true;
            }
            return false;
        }
    }
}
