﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        private MailService _mailService;
        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
            _mailService =new MailService();
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                    .ThenInclude(author => author.Avatar)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetPostById(int Id)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                    .ThenInclude(author => author.Avatar)
                .OrderByDescending(post => post.CreatedAt)
                .SingleAsync(x=> x.Id == Id);

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task<PostDTO> EditPost(PostEditDTO postEditDTO)
        {
            var postEntity = _mapper.Map<Post>(postEditDTO);
            _context.Posts.Update(postEntity);
            await _context.SaveChangesAsync();

            var editedPost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                 .FirstAsync(post => post.Id == postEntity.Id);

            var editedPostDTO = _mapper.Map<PostDTO>(editedPost);
            await _postHub.Clients.All.SendAsync("EditedPost", editedPostDTO);

            return editedPostDTO;
        }
        public async Task<bool> DeletePost(int Id , int authorId)
        {
            var deletedPost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                 .FirstAsync(post => post.Id == Id);
            if (authorId == deletedPost?.AuthorId)
            {
                if (deletedPost.PreviewId != null)
                {
                    _context.Images.Remove(_context.Images.Single(x => x.Id == deletedPost.PreviewId));
                }
                var commnets = _context.Comments.Where(x => x.PostId == deletedPost.Id);
                foreach (var comment in commnets.ToList())
                {
                    var commentReactions = _context.CommentReactions.Where(x => x.CommentId == comment.Id);
                    _context.CommentReactions.RemoveRange(commentReactions);
                }
                _context.Comments.RemoveRange(commnets);
                var postReactions = _context.PostReactions.Where(x => x.PostId == Id);
                _context.PostReactions.RemoveRange(postReactions);
                _context.Posts.Remove(deletedPost);
                await _context.SaveChangesAsync();
                
                var deletedPostDTO = _mapper.Map<PostDTO>(deletedPost);
                await _postHub.Clients.All.SendAsync("DeletedPost", deletedPostDTO);
                return true;
            }
            return false;
        }

        public async Task SharedPost(PostShareDTO postShareDTO)
        {
            var sharedPost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                 .FirstAsync(post => post.Id == postShareDTO.Id);
            string avatarUrl = _context.Images.SingleOrDefault(x => x.Id == sharedPost.Author.AvatarId)?.URL;
            string imageUrl = _context.Images.SingleOrDefault(x => x.Id == sharedPost.PreviewId)?.URL;
            User user = _context.Users.Single(x => x.Id == postShareDTO.WhoSharedUserId);
            _mailService.SendPostByMail(postShareDTO.Email, sharedPost, avatarUrl, imageUrl, user);
        }
    }
}
