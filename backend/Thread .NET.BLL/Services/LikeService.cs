﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;
using Microsoft.AspNetCore.SignalR;
using Thread_.NET.BLL.Hubs;
using Microsoft.EntityFrameworkCore;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly IHubContext<LikeHub> _likeHub;
        public MailService _mailService;

        public LikeService(ThreadContext context, IMapper mapper, IHubContext<LikeHub> likeHub) : base(context, mapper) 
        {
            _likeHub = likeHub;
            _mailService = new MailService();
        }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsLike == true);
            var dislikes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsLike == false);

            if (likes.Any())
            {
                _context.PostReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }
            if (dislikes.Any())
            {
                _context.PostReactions.RemoveRange(dislikes);
            }
            _context.PostReactions.Add(new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();

            var likedPost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                 .FirstAsync(post => post.Id == reaction.EntityId);

            var likedPostDTO = _mapper.Map<PostDTO>(likedPost);
            await _likeHub.Clients.All.SendAsync("LikePost", likedPostDTO);
            _mailService.SendMail(likedPost.Author.Email, "Like", string.Format("Your post \"{0}\" was liked by {1}", likedPost.Body, _context.Users.Single(x=> x.Id == reaction.UserId).UserName));
        }

        public async Task DislikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsLike == true);
            var dislikes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsLike == false);


            if (dislikes.Any())
            {
                _context.PostReactions.RemoveRange(dislikes);
                await _context.SaveChangesAsync();

                return;
            }
            if (likes.Any())
            {
                _context.PostReactions.RemoveRange(likes);
            }
            _context.PostReactions.Add(new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            var dislikePost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                 .FirstAsync(post => post.Id == reaction.EntityId);

            var dislikedPostDTO = _mapper.Map<PostDTO>(dislikePost);
            await _likeHub.Clients.All.SendAsync("DislikePost", dislikedPostDTO);

            await _context.SaveChangesAsync();
        }

        public async Task LikeComment(NewReactionDTO reaction)
        {
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsLike == true);
            var dislikes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsLike == false);

            if (likes.Any())
            {
                _context.CommentReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }
            if (dislikes.Any())
            {
                _context.CommentReactions.RemoveRange(dislikes);
            }
            _context.CommentReactions.Add(new DAL.Entities.CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
            await _likeHub.Clients.All.SendAsync("LikeComment", reaction);
        }

        public async Task DislikeComment(NewReactionDTO reaction)
        {
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsLike == true);
            var dislikes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsLike == false);


            if (dislikes.Any())
            {
                _context.CommentReactions.RemoveRange(dislikes);
                await _context.SaveChangesAsync();

                return;
            }
            if (likes.Any())
            {
                _context.CommentReactions.RemoveRange(likes);
            }
            _context.CommentReactions.Add(new DAL.Entities.CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
            await _likeHub.Clients.All.SendAsync("DislikeComment", reaction);

        }
    }
}
