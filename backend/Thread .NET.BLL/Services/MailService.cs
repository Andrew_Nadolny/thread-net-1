﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.ComponentModel;
using System.Configuration;
using AutoMapper.Configuration;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public class MailService
    {
        SmtpClient client;
        public MailService()
        {
            client = new SmtpClient("", 25);
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("", "");
        }
        public void SendMail(string mailTo, string subject, string message)
        {
            try
            {
                MailAddress from = new MailAddress("");
                MailAddress to = new MailAddress(mailTo);
                MailMessage mailMessage = new MailMessage(from, to);
                mailMessage.Body = message;
                mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                mailMessage.Subject = subject;
                mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
                client.Send(mailMessage);
                mailMessage.Dispose();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void SendPostByMail(string mailTo, Post post, string autrorAvatarUrl, string imageUrl ,User whoSendPost)
        {
            try
            {
                string bodyHTML = string.Format("<p><div><img src=\"{0}\"></div></p><div>{1}</div><p>{2}</p>", autrorAvatarUrl, post.Author.UserName, post.Body);
                MailAddress from = new MailAddress("");
                MailAddress to = new MailAddress(mailTo);
                MailMessage mailMessage = new MailMessage(from, to);
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = String.IsNullOrEmpty(imageUrl) ? bodyHTML : bodyHTML+ string.Format("<p><img style=\"width:400px;\" src=\"{0}\"></p>", imageUrl);
                mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                mailMessage.Subject = string.Format( "User {0} send you a post {1}", whoSendPost.UserName, post.Body);
                mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
                client.Send(mailMessage);
                mailMessage.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
