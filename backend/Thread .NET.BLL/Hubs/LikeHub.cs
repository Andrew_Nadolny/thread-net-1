﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Like;

namespace Thread_.NET.BLL.Hubs
{
    public sealed class LikeHub : Hub
    {
        public async Task Send(ReactionDTO reaction)
        {
            await Clients.All.SendAsync("NewPost", reaction);
        }
    }
}
