﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Comment;

namespace Thread_.NET.BLL.Hubs
{
    public sealed class CommentHub : Hub
    {
        public async Task Send(CommentDTO comment)
        {
            await Clients.All.SendAsync("NewComment", comment);
        }
    }
}
