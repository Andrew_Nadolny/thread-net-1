﻿using System;

namespace Thread_.NET.Common.DTO.Comment
{
    public sealed class DeletedCommentDTO
    {
        public int AuthorId { get; set; }
        public int Id { get; set; }
    }
}
