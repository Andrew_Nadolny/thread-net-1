﻿using System;

namespace Thread_.NET.Common.DTO.Comment
{
    public sealed class EditedCommentDTO
    {
        public int AuthorId { get; set; }
        public int Id { get; set; }
        public int PostId { get; set; }
        public string Body { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
