﻿using System;
using System.Collections.Generic;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Common.DTO.Post
{
    public sealed class PostShareDTO
    {
        public int Id { get; set; }
        public int WhoSharedUserId { get; set; }
        public string Email { get; set; }

    }
}
