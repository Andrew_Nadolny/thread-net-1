﻿using Newtonsoft.Json;
using System;

namespace Thread_.NET.Common.DTO.Post
{
    public sealed class PostDeleteDTO
    {
        [JsonIgnore]
        public int AuthorId { get; set; }
        public int Id { get; set; }
        
    }
}
