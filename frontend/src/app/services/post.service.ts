import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { EditedPost } from '../models/post/edited-post';
import { DeletedPost } from '../models/post/deleted-post';
import { SharedPost } from '../models/post/shared-post';



@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) { }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public editPost(post: EditedPost) {
        return this.httpService.putFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/like`, reaction);
    }

    public dislikePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/dislike`, reaction);
    }
    public deletePost(post: DeletedPost) {
        return this.httpService.deleteFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public sharePost(sharedPost: SharedPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/share`, sharedPost);
    }
}
