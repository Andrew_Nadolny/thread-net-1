import { Injectable, OnDestroy } from '@angular/core';
import { SharedDialogComponent } from '../components/share-dialog/share-dialog.component';
import { User } from '../models/user';
import { MatDialog } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Post } from 'src/app/models/post/post';

@Injectable({ providedIn: 'root' })
export class ShareDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();
    public email: string;
    public constructor(private dialog: MatDialog) { }

    public openAuthDialog(post: Post, currentUser: User) {
        const dialog = this.dialog.open(SharedDialogComponent, {
            data: { post: post, currentUser: currentUser },
            minWidth: 300,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '0'
            }
        });

        dialog
            .afterClosed()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
