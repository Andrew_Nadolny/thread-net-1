import { Component, Input, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, from, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { EditedPost } from 'src/app/models/post/edited-post';
import { DeletedPost } from 'src/app/models/post/deleted-post';
import { PostService } from '../../services/post.service';
import { GyazoService } from 'src/app/services/gyazo.service';
import { MainThreadComponent } from 'src/app/components/main-thread/main-thread.component';
import { Reaction } from 'src/app/models/reactions/reaction';
import { ShareDialogService } from 'src/app/services/share-dialog.service';



@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public showComments = false;
    public newComment = {} as NewComment;
    public editedPost = {} as EditedPost;
    public deletedPost = {} as DeletedPost;
    public imageUrl: string;
    public imageFile: File;
    public loading = false;
    public loadingPosts = false;
    public showEditPostContainer = false;
    public likes: Reaction[];
    public dislikes: Reaction[];
    public popoverIsVisibleForLikes = false;
    public popoverIsVisibleForDislikes = false;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private shareDialogService: ShareDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService: PostService,
        private gyazoService: GyazoService,
        private mainThreadComponent: MainThreadComponent
    ) { }

    public getLikesCount() {
        this.likes = this.post.reactions.filter(x => x.isLike == true);
        return this.likes.length;
    }

    public getDislikesCount() {
        this.dislikes = this.post.reactions.filter(x => x.isLike == false);
        return this.dislikes.length;
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }
        this.sortCommentArray(this.post.comments);
        this.showComments = !this.showComments;
    }

    public likePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.mainThreadComponent.likePost(this.post);
            });
    }

    public dislikePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .dislikePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.mainThreadComponent.dislikePost(this.post);
            });

    }

    public toggleEditPostContainer() {
        this.showEditPostContainer = !this.showEditPostContainer;
        this.imageUrl = this.post.previewImage;
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public openShareDialog() {
        this.shareDialogService.openAuthDialog(this.post, this.currentUser);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    public sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    public editPost(postId: any) {
        this.editedPost.Id = postId;
        this.editedPost.authorId = this.currentUser.id;
        this.editedPost.body = this.post.body;
        this.editedPost.createdAt = this.post.createdAt;
        this.editedPost.previewImage = this.post.previewImage;
        const postSubscription = !this.imageFile
            ? this.postService.editPost(this.editedPost)
            : this.gyazoService.uploadImage(this.imageFile).pipe(
                switchMap((imageData) => {
                    this.editedPost.previewImage = imageData.url;
                    return this.postService.editPost(this.editedPost);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.updateEditedPost(respPost.body);
                this.imageUrl = undefined;
                this.imageFile = undefined;
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.editedPost.Id = undefined;
                this.editedPost.authorId = undefined;
                this.editedPost.body = undefined;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
        this.toggleEditPostContainer();
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
        this.post.previewImage = undefined;
    }

    public updateEditedPost(editedPost: Post) {
        this.mainThreadComponent.editPost(editedPost);
    }

    public deletePost() {
        this.deletedPost.Id = this.post.id;
        this.deletedPost.authorId = this.currentUser.id;

        const postSubscription = this.postService.deletePost(this.deletedPost);

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.removeDeletedPost(this.post);
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public removeDeletedPost(deletedPost: Post) {
        this.mainThreadComponent.deletePost(deletedPost);
    }

    public showPopoverForCommnetLikes() {
        this.popoverIsVisibleForLikes = true;
    };

    public showPopoverForCommnetDislikes() {
        this.popoverIsVisibleForDislikes = true;
    };

    public hidePopoverForCommnetlikes() {
        this.popoverIsVisibleForLikes = false;
    };

    public hidePopoverForCommnetDislikes() {
        this.popoverIsVisibleForDislikes = false;
    };
}
