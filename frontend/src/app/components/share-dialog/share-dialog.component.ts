import { Component, OnInit, Inject, OnDestroy, Input } from '@angular/core';
import { DialogType } from '../../models/common/auth-dialog-type';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PostService } from '../../services/post.service';
import { PostComponent } from 'src/app/components/post/post.component';
import { MainThreadComponent } from 'src/app/components/main-thread/main-thread.component';
import { SharedPost } from 'src/app/models/post/shared-post';
import { Post } from 'src/app/models/post/post';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackBarService } from '../../services/snack-bar.service';


@Component({
    templateUrl: './share-dialog.component.html',
    styleUrls: ['./share-dialog.component.sass']
})
export class SharedDialogComponent implements OnInit, OnDestroy {
    @Input() public email: string;
    public dialogType = DialogType;
    public userName: string;
    public password: string;
    public avatar: string;

    public sharedPost = {} as SharedPost;
    public hidePass = true;
    public title: string;
    private unsubscribe$ = new Subject<void>();

    constructor(
        private snackBarService: SnackBarService,
        private dialogRef: MatDialogRef<SharedDialogComponent>,
        private postService: PostService,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) { }

    public ngOnInit() {
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);
    }

    public sharePost() {
        this.sharedPost.email = this.email;
        this.sharedPost.id = this.data.post.id;
        this.sharedPost.WhoSharedUserId = this.data.currentUser.id;
        this.postService.sharePost(this.sharedPost).pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.sharedPost.email = "";
                this.sharedPost.id = 0;
            },
            (error) => {
                this.snackBarService.showErrorMessage(error)
            }
        );
    }
}
