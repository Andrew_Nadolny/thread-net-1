import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { User } from '../../models/user';
import { Subject } from 'rxjs';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AuthenticationService } from '../../services/auth.service';
import { PostService } from '../../services/post.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';
import { NewPost } from '../../models/post/new-post';
import { switchMap, takeUntil } from 'rxjs/operators';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { SnackBarService } from '../../services/snack-bar.service';
import { environment } from 'src/environments/environment';
import { GyazoService } from 'src/app/services/gyazo.service';
import { Comment } from 'src/app/models/comment/comment';
import { PostComponent } from 'src/app/components/post/post.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrService } from 'ngx-toastr';



@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass']
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public isOnlyMine = false;
    public isFavotie = false;


    public currentUser: User;
    public imageUrl: string;
    public imageFile: File;
    public post = {} as NewPost;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;

    public postHub: HubConnection;
    public commentHub: HubConnection;
    public postComponent: PostComponent;


    private unsubscribe$ = new Subject<void>();

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private gyazoService: GyazoService,
        private authDialogService: AuthDialogService,
        private eventService: EventService,
        private toasterService: ToastrService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.postHub.stop();
        this.commentHub.stop();
    }

    public ngOnInit() {
        this.registerHub();
        this.getPosts();
        this.getUser();

        this.eventService.userChangedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
            this.currentUser = user;
            this.post.authorId = this.currentUser ? this.currentUser.id : undefined;
        });
    }

    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.posts = this.cachedPosts = resp.body;
                },
                (error) => (this.loadingPosts = false)
            );
    }

    public sendPost() {
        const postSubscription = !this.imageFile
            ? this.postService.createPost(this.post)
            : this.gyazoService.uploadImage(this.imageFile).pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.url;
                    return this.postService.createPost(this.post);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                //this.addNewPost(respPost.body);
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
                this.toasterService.success("New post added", 'Success');
            },
            (error) => {
                this.snackBarService.showErrorMessage(error)
                this.toasterService.error(error, 'Error');
            }
        );
    }


    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public sliderChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.isOnlyMine = true;
            if (this.isFavotie) {
                this.posts = this.cachedPosts.filter((x) => x.author.id === this.currentUser.id).filter((x) => x.reactions.find(x => x.user.id === this.currentUser.id)?.isLike === true);
            }
            else {
                this.posts = this.cachedPosts.filter((x) => x.author.id === this.currentUser.id);
            }
        } else {
            this.isOnlyMine = false;
            if (this.isFavotie) {
                this.posts = this.cachedPosts.filter((x) => x.reactions.find(x => x.user.id === this.currentUser.id)?.isLike === true);
            }
            else {
                this.posts = this.cachedPosts;
            }
        }
    }

    public sliderFavoriteChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.isFavotie = true;
            if (this.isOnlyMine) {
                this.posts = this.cachedPosts.filter((x) => x.author.id === this.currentUser.id).filter((x) => x.reactions.find(x => x.user.id === this.currentUser.id)?.isLike === true);
            }
            else {
                this.posts = this.cachedPosts.filter((x) => x.reactions.find(x => x.user.id === this.currentUser.id)?.isLike === true);
            }

        } else {
            this.isFavotie = false;
            if (this.isOnlyMine) {
                this.posts = this.cachedPosts.filter((x) => x.author.id === this.currentUser.id);
            }
            else {
                this.posts = this.cachedPosts;
            }
        }
    }

    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    public registerHub() {
        this.postHub = new HubConnectionBuilder().withUrl(`${environment.apiUrl}/notifications/post`).build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.commentHub = new HubConnectionBuilder().withUrl(`${environment.apiUrl}/notifications/comment`).build();
        this.commentHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.postHub.on('NewPost', (newPost: Post) => {
            if (newPost) {
                this.addNewPost(newPost);
            }
        });

        this.postHub.on('EditedPost', (editedPost: Post) => {
            if (editedPost) {
                this.editPost(editedPost);
            }
        });

        this.postHub.on('DeletedPost', (deletedPost: Post) => {
            if (deletedPost) {
                this.deletePost(deletedPost);
            }
        });

        this.commentHub.on('NewComment', (newComment: Comment) => {
            if (newComment) {
                this.posts.find(x => x.id == newComment.postId).comments = this.sortCommentArray(this.posts.find(x => x.id == newComment.postId).comments.concat(newComment));
            }
        });

        this.commentHub.on('EditedComment', (editedComment: Comment) => {
            if (editedComment) {
                this.posts.find(x => x.id == editedComment.postId).comments = this.editComment(editedComment);
            }
        });

        this.commentHub.on('DeletedComment', (deletedComment: Comment) => {
            if (deletedComment) {
                this.posts.find(x => x.id == deletedComment.postId).comments = this.deleteComment(deletedComment);
            }
        });
    }

    public editComment(editedCommnet: Comment) {
        this.posts.find(x => x.id == editedCommnet.postId).comments.splice(this.posts.find(x => x.id == editedCommnet.postId).comments.findIndex(x => x.id === editedCommnet.id), 1, editedCommnet)
        return this.posts.find(x => x.id == editedCommnet.postId).comments = this.sortCommentArray(this.posts.find(x => x.id == editedCommnet.postId).comments);
    }

    public deleteComment(deletedCommnet: Comment) {
        this.posts.find(x => x.id == deletedCommnet.postId).comments.splice(this.posts.find(x => x.id == deletedCommnet.postId).comments.findIndex(x => x.id === deletedCommnet.id), 1)
        return this.posts.find(x => x.id == deletedCommnet.postId).comments = this.sortCommentArray(this.posts.find(x => x.id == deletedCommnet.postId).comments);
    }

    public addNewPost(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));
            if (!this.isOnlyMine || (this.isOnlyMine && newPost.author.id === this.currentUser.id)) {
                this.posts = this.sortPostArray(this.posts.concat(newPost));
            }
        }
    }

    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }

    private sortPostArray(array: Post[]): Post[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    public editPost(editedPost: Post) {
        if (this.cachedPosts.some((x) => x.id === editedPost.id)) {
            this.cachedPosts.splice(this.cachedPosts.findIndex(x => x.id === editedPost.id), 1, editedPost)
            this.cachedPosts = this.sortPostArray(this.cachedPosts);
            if (!this.isOnlyMine || (this.isOnlyMine && editedPost.author.id === this.currentUser.id)) {
                this.posts.splice(this.posts.findIndex(x => x.id === editedPost.id), 1, editedPost);
                this.posts = this.sortPostArray(this.posts);
            }
            if (!this.isFavotie || (this.isFavotie && editedPost.author.id === this.currentUser.id)) {
                this.posts.splice(this.posts.findIndex(x => x.id === editedPost.id), 1, editedPost);
                this.posts = this.sortPostArray(this.posts);
            }
        }
    }

    public deletePost(deletedPost: Post) {
        if (this.cachedPosts.some((x) => x.id === deletedPost.id)) {
            this.cachedPosts.splice(this.cachedPosts.findIndex(x => x.id === deletedPost.id), 1);
            this.cachedPosts = this.sortPostArray(this.cachedPosts);
            if (!this.isOnlyMine || (this.isOnlyMine && deletedPost.author.id === this.currentUser.id)) {
                this.posts.splice(this.posts.findIndex(x => x.id === deletedPost.id), 1);
                this.posts = this.sortPostArray(this.posts);
            }
            if (!this.isFavotie || (this.isFavotie && deletedPost.author.id === this.currentUser.id)) {
                this.posts.splice(this.posts.findIndex(x => x.id === deletedPost.id), 1);
                this.posts = this.sortPostArray(this.posts);
            }
        }
    }

    public likePost(likedPost: Post) {
        if (this.isFavotie) {
            this.posts.splice(this.posts.findIndex(x => x.id === likedPost.id), 1);
            this.posts = this.sortPostArray(this.posts);
        }
    }

    public dislikePost(dislikedPost: Post) {
        if (this.isFavotie) {
            this.posts.splice(this.posts.findIndex(x => x.id === dislikedPost.id), 1);
            this.posts = this.sortPostArray(this.posts);
        }
    }
}
