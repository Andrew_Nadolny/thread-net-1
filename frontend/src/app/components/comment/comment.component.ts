import { Component, Input, OnDestroy } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { EditedComment } from '../../models/comment/edited-comment';
import { DeletedComment } from '../../models/comment/deleted-comment';
import { CommentService } from '../../services/comment.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { PostComponent } from 'src/app/components/post/post.component';
import { LikeService } from '../../services/like.service';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { Reaction } from 'src/app/models/reactions/reaction';


@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnDestroy {
    @Input() public comment: Comment;
    public currentUser = this.postComponent.currentUser;
    public editedComment = {} as EditedComment;
    public deletedComment = {} as DeletedComment;
    public showEditCommentContainer = false;
    private unsubscribe$ = new Subject<void>();
    public popoverIsVisibleForLikes = false;
    public popoverIsVisibleForDislikes = false;
    public likes: Reaction[];
    public dislikes: Reaction[];


    public constructor(
        private authService: AuthenticationService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postComponent: PostComponent,
        private likeService: LikeService,
        private authDialogService: AuthDialogService,) {
    }

    public editComment() {
        this.editedComment.Id = this.comment.id;
        this.editedComment.authorId = this.comment.author.id;
        this.editedComment.postId = this.postComponent.post.id;
        this.editedComment.body = this.comment.body;
        this.editedComment.createdAt = this.comment.createdAt;
        this.commentService.editComment(this.editedComment).pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
        this.toggleEditCommentContainer();
    }

    public deleteComment() {
        this.deletedComment.Id = this.comment.id;
        this.deletedComment.authorId = this.comment.author.id;
        this.commentService.deleteComment(this.deletedComment).pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleEditCommentContainer() {
        this.showEditCommentContainer = !this.showEditCommentContainer;
    }

    public likeComment() {
        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public dislikeComment() {
        this.likeService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public getLikesCount() {
        this.likes = this.comment.reactions.filter(x => x.isLike == true);
        return this.likes.length;
    }

    public getDislikesCount() {
        this.dislikes = this.comment.reactions.filter(x => x.isLike == false);
        return this.dislikes.length;
    }

    public showPopoverForCommnetLikes() {
        this.popoverIsVisibleForLikes = true;
    };

    public showPopoverForCommnetDislikes() {
        this.popoverIsVisibleForDislikes = true;
    };

    public hidePopoverForCommnetlikes() {
        this.popoverIsVisibleForLikes = false;
    };

    public hidePopoverForCommnetDislikes() {
        this.popoverIsVisibleForDislikes = false;
    };
}


