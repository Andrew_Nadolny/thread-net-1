import { User } from '../user';
import { Comment } from '../comment/comment';
import { Reaction } from '../reactions/reaction';

export interface SharedPost {
    email: string,
    WhoSharedUserId: number;
    id: number;
}