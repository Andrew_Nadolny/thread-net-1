export interface EditedPost {
    Id: number;
    authorId: number;
    body: string;
    previewImage: string;
    createdAt: Date;
}
