export interface EditedComment {
    authorId: number;
    Id: number;
    postId: number;
    body: string;
    createdAt: Date;
}
